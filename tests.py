#!/usr/bin/env python
# -*- coding: windows-1251 -*-

import unittest

from pwars.fleet import Fleet
from pwars.planet import Planet
from pwars.universe import Universe

class GameStub():
  def log(self, msg):
    pass

class Test(unittest.TestCase):

#  def tearDown(self):
  def setUp(self):
    self.game = GameStub()
    self.uni = Universe(self.game)

    self.uni.update("P  3.5 4.5 1 10 3") # dist 0 <-> 2 = 6
    self.uni.update("#P 3.5 4.5 1 10 3")
    self.uni.update("P  5.5 6.5 2 11 3")
    self.uni.update("P  7.5 8.5 0 12 5")

    self.uni.turn_start(1)

  def test_Fleet(self):
    f = Fleet(self.uni, "1", "5", "1", "2", "7", "6")
    self.assertEqual(f.owner, 1)
    self.assertEqual(f.ship_count, 5)
    self.assertEqual(f.source, 1)
    self.assertEqual(f.destination, 2)
    self.assertEqual(f.trip_length, 7)
    self.assertEqual(f.turns_remaining, 6)

  def test_Planet(self):
    p = Planet(self.uni, 0, "5.5", "4.5", "1", "10", "2")
    self.assertEqual(p.id, 0)
    self.assertEqual(p.x, 5.5)
    self.assertEqual(p.y, 4.5)
    self.assertEqual(p.owner, 1)
    self.assertEqual(p.ship_count, 10)
    self.assertEqual(p.growth_rate, 2)

  def test_Universe(self):
    self.assertEqual(len(self.uni.planets.keys()), 3)
    self.assertEqual(len(self.uni.planets_my), 1)
    self.assertEqual(len(self.uni.planets_enemy), 1)
    self.assertEqual(len(self.uni.planets_neitral), 1)
    self.assertEqual(len(self.uni.planets_not_my), 2)
    self.assertEqual(self.uni.planets[self.uni.planets_my[0]].ship_count, 10)

  def test_Universe_update(self):
    self.uni.turn_done()
    self.uni.update("P 3.5 4.5 1 13 3")
    self.uni.update("P 5.5 6.5 2 14 3")
    self.uni.update("P 7.5 8.5 0 17 5")

    self.assertEqual(len(self.uni.planets.keys()), 3)
    self.assertEqual(len(self.uni.planets_my), 1)
    self.assertEqual(len(self.uni.planets_enemy), 1)
    self.assertEqual(len(self.uni.planets_neitral), 1)
    self.assertEqual(len(self.uni.planets_not_my), 2)

    self.assertEqual(self.uni.planets[self.uni.planets_my[0]].ship_count, 13)
    self.assertEqual(self.uni.planets[self.uni.planets_enemy[0]].ship_count, 14)
    self.assertEqual(self.uni.planets[self.uni.planets_neitral[0]].ship_count, 17)

  def test_Planet_Battle(self):
    from pwars.planet import Battle

    owner, ships = Battle([0, 0, 0])
    self.assertEqual(owner, -1)
    self.assertEqual(ships, 0)

    owner, ships = Battle([0, 15, 15])
    self.assertEqual(owner, -1)
    self.assertEqual(ships, 0)

    owner, ships = Battle([15, 15, 15])
    self.assertEqual(owner, -1)
    self.assertEqual(ships, 0)

    owner, ships = Battle([10, 15, 15])
    self.assertEqual(owner, -1)
    self.assertEqual(ships, 0)

    owner, ships = Battle([0, 0, 15])
    self.assertEqual(owner, 2)
    self.assertEqual(ships, 15)

    owner, ships = Battle([10, 11, 15])
    self.assertEqual(owner, 2)
    self.assertEqual(ships, 4)

  def test_Planet_attack(self):
    self.uni.turn_done()

    self.uni.update("P 3.5 4.5 1 10 3") # 0
    self.uni.update("P 5.5 6.5 2 10 3") # 1
    self.uni.update("P 7.5 8.5 0 10 5") # 2

    # ����� ����������� ���� ������� (id=0, �������� 10) 5 ��������� � 4 ����
    mp = self.uni.planets[self.uni.planets_my[0]]
    self.uni.update("F 2 5 1 0 5 4") 
    self.assertEqual(mp.isUnderAttack(), True)
    self.assertEqual(mp.ETA_enemy[4], 5)

    # ����� ����������� ���� ������� (id=0, �������� 10) 6 ��������� � 5 �����
    self.uni.update("F 2 6 2 0 6 5") 
    self.assertEqual(mp.ETA_enemy[5], 6)

    # �� ������� 10 ��������, ������������ 3.
    # ����� 4 ���� �������� 5 ������, ���� ����� 10 + 3 * 4 = 22, ��������� ��������� 22 - 5 = 17 ����.
    # ��� ����� ��� �������� 6 ������, ���� ����� 17 + 3 = 20, ��������� 14 ����
    # ������� ��������� �� �����.
    future = mp.getFuture()
    self.assertEqual(mp.isOwnerChange(future), False)

    # ��������� � ����� ���������� ���� ������� (id=0, �������� 10) 20 �������� � 6 �����
    self.uni.update("F 2 20 2 0 7 6") 
    self.assertEqual(mp.ETA_enemy[6], 20)

    # ����� ���������� ����� ��������� 14 ����
    # �� ��������� ��� �������� 20 ������. ���� 14 + 3 � ������������. ��������� 17 - 20 = 3 �����
    # ������� ����� ��������� ����� 6 �����. ��� ������ ����� �� 6 ����� ��������� �� ������� 3 �������.
    future = mp.getFuture()
    self.assertEqual(mp.isOwnerChange(future), True)
    owner, ships = future[6]
    self.assertEqual(owner, 2)
    self.assertEqual(ships, 3)

    # ��������� � ������ ���� ������� (id=0, �������� 10) 3 ������� � 6 �����
    # ������ �������� 0 �������� � ������� �������� ����
    self.uni.update("F 1 3 2 0 7 6") 
    future = mp.getFuture()
    self.assertEqual(mp.isOwnerChange(future), False)

  def test_Planet_enemy_support(self):
    self.uni.turn_done()

    self.uni.update("P 3.5 4.5 1 10 3") # 0
    self.uni.update("P 5.5 6.5 2 10 3") # 1
    self.uni.update("P 7.5 8.5 0 10 5") # 2
    # enemy send fleet to own planet
    self.uni.update("F 2 5 0 1 5 4") 

    p = self.uni.planets[self.uni.planets_enemy[0]]
    self.assertEqual(p.isUnderAttack(), False)
    future = p.getFuture()
    self.assertEqual(p.isOwnerChange(future), False)

  def test_Planet_neutral_attack_by_me(self):
    self.uni.turn_done()

    self.uni.update("P 3.5 4.5 1 10 3") # 0
    self.uni.update("P 5.5 6.5 2 10 3") # 1
    self.uni.update("P 7.5 8.5 0 10 5") # 2
    self.uni.update("F 1 15 0 2 5 4") 

    p = self.uni.planets[self.uni.planets_neitral[0]]
    self.assertEqual(p.isUnderAttack(), True)
    future = p.getFuture()
    self.assertEqual(p.isOwnerChange(future), True)
    owner, ships = future[4]
    self.assertEqual(owner, 1)
    self.assertEqual(ships, 5)

  def test_Planet_neutral_attack_by_enemy(self):
    self.uni.turn_done()

    self.uni.update("P 3.5 4.5 1 10 3") # 0
    self.uni.update("P 5.5 6.5 2 10 3") # 1
    self.uni.update("P 7.5 8.5 0 10 5") # 2
    # enemy send fleet to neutral planet
    self.uni.update("F 2 15 0 2 5 4") 

    p = self.uni.planets[self.uni.planets_neitral[0]]
    self.assertEqual(p.isUnderAttack(), True)
    future = p.getFuture()
    self.assertEqual(p.isOwnerChange(future), True)
    owner, ships = future[4]
    self.assertEqual(owner, 2)
    self.assertEqual(ships, 5)

  def test_Planet_neutral_attack_by_my_and_enemy(self):
    self.uni.turn_done()

    self.uni.update("P 3.5 4.5 1 10 3") # 0
    self.uni.update("P 5.5 6.5 2 10 3") # 1
    self.uni.update("P 7.5 8.5 0 10 5") # 2

    self.uni.update("F 2 15 1 2 5 4") 
    self.uni.update("F 1 15 0 2 5 4") 

    p = self.uni.planets[self.uni.planets_neitral[0]]
    self.assertEqual(p.isUnderAttack(), True)
    future = p.getFuture()
    self.assertEqual(p.isOwnerChange(future), False)
    owner, ships = future[4]
    self.assertEqual(owner, 0)
    self.assertEqual(ships, 0)

    self.uni.turn_done()

    self.uni.update("P 3.5 4.5 1 10 3") # 0
    self.uni.update("P 5.5 6.5 2 10 3") # 1
    self.uni.update("P 7.5 8.5 0 10 5") # 2

    self.uni.update("F 2 20 1 2 5 4") 
#    self.uni.update("F 1 15 0 2 6 5") 
    self.uni.send_fleet(0, 2, 20)
    v = {}
    v[0] = 5

    p = self.uni.planets[self.uni.planets_neitral[0]]
    self.assertEqual(p.isUnderAttack(), True)
    future = p.getFuture(v)

    self.assertEqual(p.isOwnerChange(future), True)
    owner, ships = future[4]
    self.assertEqual(owner, 2)
    self.assertEqual(ships, 10)
    owner, ships = future[6]
    self.assertEqual(owner, 1)
    self.assertEqual(ships, 5)

  def test_lastBattleResult(self):
    self.uni.turn_done()

    self.uni.update("P 3.5 4.5 1 12 3") # 0
    self.uni.update("P 5.5 6.5 2 10 3") # 1
    self.uni.update("P 7.5 8.5 0 10 5") # 2

    v = {}
    v[0] = 11
    p = self.uni.planets[self.uni.planets_neitral[0]]
    future = p.getFuture(v)
    (turn, owner, ships) = p.lastBattleResult(future)
    self.assertEqual(owner, 1)
    self.assertEqual(ships, 1)
    self.assertEqual(turn, 6)

# F <owner:int> <ships:int> <source:int> <destination:int> <total_turns:int> <remaining_turns:int>
# P <x:float> <y:float> <owner:int> <ships:int> <growth:int>
def testSuite():
  return unittest.makeSuite(Test, 'test')

if __name__ == '__main__': 
  unittest.main(defaultTest='testSuite')

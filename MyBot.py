#!/usr/bin/env python
# -*- coding: windows-1251 -*-

from pwars import BaseBot, Game

def CanWin (u, p):

  future = p.getFuture()
  (turn, owner, ships) = p.lastBattleResult(future)

  if (owner == 1) or ((owner == -1) and (p.owner == 1)):
    return {}

  v = {}
  for pid in p.nearestPlanets:
    ps = u.planets[pid]
    if (ps.owner == 1) and (ps.freeShip > 0) and (p.distanceTo[pid] < int(u.max_dist * 0.5)):
      v[pid] = ps.freeShip
      future = p.getFuture(v)
      (turn, owner, ships) = p.lastBattleResult(future)

      if owner == 1:
        if ships > 0:
          tot = ps.freeShip - ships + 1
 
          if (tot > 0) and (tot < ps.freeShip):
            v[pid] = tot
          else:
            u.game.log("!!! CanWin ERR: %d from %d. turn: %d owner %d ships %d future %s." % (tot, pid, turn, owner, ships, repr(future)))

        return v
  return {}

def calcROI (u, p):

  v = CanWin(u, p) # ��������� ����������� �������� �����
  if len(v.keys()) == 0:
    return -1

  if p.growth_rate == 0:
    return 0

  future = p.getFuture(v)
  (turn, owner, ships) = p.lastBattleResult(future)
  ships = 0
  for x in v.keys():
    ships += v[x]

  if p.owner == 0:
    ships += p.ship_count

  invest = float(ships) / float(p.growth_rate) + turn
  return float(1000) / invest

class Gena_the_crocodile(BaseBot):

  def do_turn(self, turn_count):
    u = self.universe
    g = u.game

    # ��������� ROI ��� ������
    maxRoi = -1
    for pid in u.planets.keys():
      p = u.planets[pid]
      p.ROI = calcROI (u, p)
      if maxRoi < p.ROI:
        maxRoi = p.ROI

    g.log("Max.dist %d Planets my: %d enemy: %d neitral: %d ROI: %.3f" % (u.max_dist, len(u.planets_my), len(u.planets_enemy), len(u.planets_neitral), maxRoi))

    # ���� ������.
    ########################

    for pid in u.planets_my:
      p = u.planets[pid]
      if p.isUnderAttack(): # ����� ���� ��������� �������

        g.log("My under attack: %s" % p.verbose())
        future = p.getFuture()

        owner, min_ships = (p.owner, p.ship_count)
        min_turn = 0
        last_turn = 0
        last_ships = 0
        prev_owner = p.owner

        for turn in sorted(future.keys()):
          owner, ships = future[turn]

          if (owner == 2) and (prev_owner != owner):
            last_turn = turn
            last_ships = ships

          if (owner == 1) and (ships < min_ships):
            min_ships = ships
            min_turn = turn

          prev_owner = owner

        if p.owner != owner: # ������� �� ������ �������� ��������������
          g.log("Will be captured: %d turns %d ships" % (last_turn, last_ships))
          p.freeShip = 0
        else: # ��� ��� ������, ������� ����� �������� ����, ��������, ������� �������� � ��� ����� ������� ��� ������
          g.log("Max drama: %d turns %d ships" % (min_turn, min_ships))
          p.freeShip = min_ships

    # ������ ����������. �� ����� �������� ����� ������� ������� ��������, ����� �������� ���� ���� �������� � ��������� ������� ����������
    my = u.home_my # ��� ��������� �������

    en = u.home_en # ��������� ��������� �������
    for pid in my.nearestPlanets:
      ps = u.planets[pid]
      if ps.owner == 2:
        en = ps

    free_ship = my.freeShip - (en.ship_count - my.growth_rate * my.distanceTo[en.id])
    if free_ship < 0:
  
      my.freeShip = 0
      g.log("Special homeland rule. Deficit %d ships." % -free_ship)
  
      free_ship = -free_ship
      for pid in my.nearestPlanets:
        ps = u.planets[pid]
        if (ps.owner == 1) and (ps.freeShip > 0) and (my.distanceTo[pid] <= my.distanceTo[en.id]):
          if ps.freeShip < free_ship:
            g.log("Defense homeland partial")
            g.send_fleet(pid, my.id, ps.freeShip)
            free_ship -= ps.freeShip
          else:
            g.log("Defense homeland complete")
            g.send_fleet(pid, my.id, free_ship)
            free_ship = 0
            break
      if free_ship > 0:
        g.log("Homeland dunger! Deficit %d ships." % free_ship)

    else:

      if my.freeShip > free_ship:
        my.freeShip = free_ship

    # ������� ���� ��� ����� �����
    target_planets = sorted(u.planets.values(), key=lambda x: x.ROI, reverse=True)

    for p in target_planets:
      if p.ROI < 0: continue
      v = CanWin(u, p) # ��������� ����������� �������� �����
      if len(v.keys()) > 0: # ������� ������ ���-�� ��������
        g.log("New attack %s." % str(p))
        for pid in v.keys():
          ps = u.planets[pid]
          g.send_fleet(pid, p.id, v[pid])
      else:
        g.log("Can not new attack %s." % str(p))

    # ����������� ���������� ��������� ������� ����� � ���������
    g.log("Move free ships.")
    for pid in u.planets_my:
      p = u.planets[pid]
      if p.freeShip > 0:
        trg = p.route2enemy()
        if trg >=0:
          g.send_fleet(pid, trg, p.freeShip)

Game(Gena_the_crocodile)

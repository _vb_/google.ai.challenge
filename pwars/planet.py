from math import ceil, sqrt

planet_owner_string = ["Ne", "My", "En"]

def Battle(forces):
  number_one = max(forces) # get srongest force value
  number_two = min(forces)
  srongest_count = 0

  for force in forces:
    if force == number_one: # count srongest forces
      srongest_count += 1
    else: # find next after strongest
      if force > number_two: number_two = force

  if srongest_count > 1: # more then one srongest force -> draw
    return (-1, 0)

  # return winner index in forces and ships remainder
  return (forces.index(number_one), number_one - number_two)

class Planet(object):

  def __init__(self, universe, id, x, y, owner, ship_count, growth_rate):
    self.universe = universe
    self.id = int(id)
    self.x = float(x)
    self.y = float(y)
    self.owner = int(owner)
    self.ship_count = int(ship_count)
    self.growth_rate = int(growth_rate)

    self.ROI = -1
    self.distanceTo = {}
    self.ETA_clear()
    self.initTurnData()

  def __repr__(self):
    return "<%s(%d) #%d +%d ROI %.3f free %d>" % (planet_owner_string[self.owner], self.id, self.ship_count, self.growth_rate, self.ROI, self.freeShip)

  def verbose(self):
    s = ""
    if self.isUnderAttack():
      s += "Attacked:"
      for eta in sorted(self.ETA_my.keys() + map(lambda x: -x, self.ETA_enemy.keys()), key=lambda x: abs(x)):
        ships = 0
        if eta < 0:
          ships = -self.ETA_enemy[-eta]
        else:
          ships = self.ETA_my[eta]
        s += " ETA %d-> %d" % (abs(eta), ships)

    return "%s: %s" % (str(self), s)

  def update(self, owner, ship_count):
    self.owner = int(owner)
    self.ship_count = int(ship_count)
    self.initTurnData()

  def initTurnData(self):
    self.freeShip = self.ship_count

  def ETA_add(self, fleet):
    data = self.ETA_my if (fleet.owner == 1) else self.ETA_enemy
    if fleet.turns_remaining in data:
      data[fleet.turns_remaining] += fleet.ship_count
    else:
      data[fleet.turns_remaining] = fleet.ship_count

  def ETA_clear(self):
    self.ETA_enemy = {}
    self.ETA_my = {}

  def isUnderAttack(self):
    if self.owner == 2:
      return len(self.ETA_my.keys()) > 0 
    if self.owner == 1:
      return len(self.ETA_enemy.keys()) > 0 

    return len(self.ETA_my.keys() + self.ETA_enemy.keys()) > 0 

  def RangePlanetDistances(self):
    self.nearestPlanets = sorted(self.distanceTo.keys(), key=lambda x: self.distanceTo[x])

  def getFuture(self, variants={}):
    future = {}
    defense_ships = self.ship_count
    curent_owner = self.owner
    prev_eta = 0

    my_var = {}
    for pid in variants.keys():
      turn = self.distanceTo[pid]
      if turn in my_var:
        my_var[turn] += variants[pid]
      else:
        my_var[turn] = variants[pid]

    for turn in self.ETA_my.keys():
      if turn in my_var:
        my_var[turn] += self.ETA_my[turn]
      else:
        my_var[turn] = self.ETA_my[turn]

    etas = sorted(my_var.keys() + map(lambda x: -x, self.ETA_enemy.keys()), key=lambda x: abs(x)) 

    i = 0
    while i < len(etas):

      eta = etas[i]

      if curent_owner > 0: # no production on neutral
        defense_ships += (abs(eta) - prev_eta) * self.growth_rate

      my_ships = 0
      en_ships = 0
      ne_ships = 0
      if eta > 0:
        my_ships += my_var[eta]
      else:
        en_ships += self.ETA_enemy[-eta]

      # check out the simultaneous arrival of several fleets
      while ((i+1) < len(etas)) and (abs(etas[i+1]) == abs(eta)):
        i += 1
        eta = etas[i]
        if eta > 0:
          my_ships += my_var[eta]
        else:
          en_ships += self.ETA_enemy[-eta]

      # Force for battle
      if curent_owner == 0:
        ne_ships += defense_ships
      elif curent_owner == 1:
        my_ships += defense_ships
      else:
        en_ships += defense_ships
      # Battle!
      owner, defense_ships = Battle([ne_ships, my_ships, en_ships])
      # Batle result
      if owner > 0:
        curent_owner = owner
      future[abs(eta)] = (curent_owner, defense_ships)

      prev_eta = abs(eta)
      i += 1

    return future

  def lastBattleResult(self, future):
    battles = sorted(future.keys())
    indx = len(battles) - 1
    if indx < 0:
      return (-1, -1, -1)
    result_owner, ships = future[battles[indx]]
    return (battles[indx], result_owner, ships)

  def isOwnerChange(self, future):
    indx, result_owner, ships = self.lastBattleResult(future)
    if indx < 0:
      return False
    return result_owner != self.owner

  def route2enemy (self):
    for pid in self.nearestPlanets:
      p = self.universe.planets[pid]
      if p.owner == 2:
        for pidm in self.nearestPlanets:
          pm = self.universe.planets[pidm]
          if (pm.owner == 1) and (pm.distanceTo[pid] < self.distanceTo[pid]):
            return pidm
    return -1

  def distance(self, other):
    """Returns the distance to <other>. <other> must be one of Planet instance, list, tuple or Point."""
    if isinstance(other, Planet):
      ox = other.x
      oy = other.y
    elif isinstance(other, (list, tuple)):
      ox = other[0]
      oy = other[1]
    dx = self.x - ox
    dy = self.y - oy
    return int(ceil(sqrt(dx ** 2 + dy ** 2)))

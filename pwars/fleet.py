
class Fleet(object):
    def __init__(self, universe, owner, ship_count, source, destination, trip_length, turns_remaining):
        self.universe = universe
        self.owner = int(owner)
        self.ship_count = int(ship_count)
        self.source = int(source)
        self.destination = int(destination)
        self.trip_length = int(trip_length)
        self.turns_remaining = int(turns_remaining)

    def __repr__(self):
        return "<F(%d) #%d %s -> %s ETA %d>" % (self.owner, self.ship_count, self.source, self.destination, self.turns_remaining)


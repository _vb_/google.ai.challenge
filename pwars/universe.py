from pwars.fleet import Fleet
from pwars.planet import Planet

class Universe(object):
  """'Main' planetwars object. The Universe knows about all planets and fleets and talks to the Game.
  The planets objects are stable. That means you can keep references to Planet objects in you own code and
  they will still be valid in the next turn
  """

  def __init__(self, game):
    self.game = game
    self.planets = {}
    self.needMeasure = False
    self.turn_done()

  def update(self, game_state_line):
    """Update the game state. Gets called from Game."""
    line = game_state_line.split("#")[0]
    tokens = line.split()

    if len(tokens) < 6:
      # Garbage - ignore
      return

    if tokens[0] == "P":

      if len(tokens) != 6: #Invalid format in planet
        return

      planet = 0
      try :
        planet = self.planets[self.planet_id]
      except:
        planet = Planet(self, self.planet_id, *tokens[1:])
        self.planets[self.planet_id] = planet
        self.needMeasure = True

      planet.update(tokens[3], tokens[4])

      if planet.owner == 1:
        self.planets_my.append(self.planet_id)
      elif planet.owner == 2:
        self.planets_enemy.append(self.planet_id)
        self.planets_not_my.append(self.planet_id)
      else:
        self.planets_neitral.append(self.planet_id)
        self.planets_not_my.append(self.planet_id)

      self.planet_id += 1

    elif tokens[0] == "F":

      if len(tokens) != 7: #Invalid format in fleet
        return

      fleet = Fleet(self, *tokens[1:])
      self.planets[fleet.destination].ETA_add(fleet)
      if fleet.owner == 1:
        self.fleets_my.append(fleet)
      else:
        self.fleets_enemy.append(fleet)

  def Measure(self):
    self.max_dist = 0
    for pid in self.planets.keys():
      self.CalcPlanetDist(pid)
    self.home_my = self.planets[self.planets_my[0]] # my home planet
    self.home_en = self.planets[self.planets_enemy[0]] # enemy home planet

  def CalcPlanetDist(self, pid):
    p = self.planets[pid]
    for pid1 in self.planets.keys():
      if (pid != pid1) and (pid1 not in p.distanceTo):
        p1 =self.planets[pid1]
        dst = p.distance(p1)
        p.distanceTo[pid1] = dst
        p1.distanceTo[pid] = dst
        if dst > self.max_dist:
          self.max_dist = dst
    p.RangePlanetDistances()

  def turn_start(self, turn_count):
    if self.needMeasure:
      self.Measure()
      self.needMeasure = False

    self.planets_my.sort(key=lambda x: self.planets[x].growth_rate, reverse=True)
    self.planets_enemy.sort(key=lambda x: self.planets[x].growth_rate, reverse=True)
    self.planets_neitral.sort(key=lambda x: self.planets[x].growth_rate, reverse=True)
    self.planets_not_my.sort(key=lambda x: self.planets[x].growth_rate, reverse=True)

  def send_fleet(self, source_id, destination_id, ship_count):
    """Record fleets to send so we can aggregate them."""
    key = "%d.%d" % (source_id, destination_id)
    if key in self._fleets_to_send:
      self._fleets_to_send[key][2] += ship_count
    else:
      self._fleets_to_send[key] = [source_id, destination_id, ship_count]

    p = self.planets[destination_id]
    dst = p.distanceTo[source_id]
    p.ETA_add(Fleet(self, 1, ship_count, source_id, destination_id, dst, dst))

  def turn_done(self):
    self.planet_id = 0
    self.fleets_my = []
    self.fleets_enemy = []
    self.planets_my = []
    self.planets_enemy = []
    self.planets_neitral = []
    self.planets_not_my = []
    self._fleets_to_send = {}
    for pid in self.planets.keys():
      self.planets[pid].ETA_clear()

# Copyright (c) 2010 Vitaly Bogomolov <vit.sar68@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from game import Game
from basebot import BaseBot

__doc__ = """pwars

This is a Python "toolkit" for the second Google AI Contest (http://ai-constest.com).
Based on "toolkit" from Ulrich Petri <mail@ulo.pe>. Big thanks, Ulrich!

Usage:
Create a class for your bot that inherits from BaseBot and implements a do_turn() method.
Then instantiate Game with your bot class name as argument.

e.g.:

>>> from pwars import BaseBot, Game
>>> from random import choice
>>>
>>> class MyBot(BaseBot):
...   def do_turn(turn_number):
...     u = self.universe
...     g = u.game
...     for pid in u.planets_my:
...       p = u.planets[pid]
...       if p.ship_count > 50:
...         g.send_fleet(pid, random.choice(u.planets_not_my), p.ship_count / 2)
>>>
>>> Game(MyBot)
>>>
"""

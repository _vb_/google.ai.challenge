import logging
import sys
from time import time
from pwars.universe import Universe

class Game(object):
  """The Game object talks to the tournament engine and updates the universe.
  You should instantiate it with your BotClass as first argument, e.g:
  >>> Game(MyBot)
  """
  def __init__(self, bot_class):

    self.logger = logging.getLogger(__name__)
    logging.basicConfig(filename="pwars.log", level=logging.DEBUG, format="%(message)s")

    self.universe = Universe(self)
    self.bot = bot_class(self.universe)
    self.turn_count = 0
        
    self.log("----------- GAME START -----------\n")
    try:
      while True:
        line = sys.stdin.readline().strip()
        if line.startswith("go"):
          self.turn_count += 1
          self.log("=== TURN START === (Turn no: %d)" % self.turn_count)
          turn_start = time()
          self.universe.turn_start(self.turn_count)
          self.bot.do_turn(self.turn_count)
          self.log("### TURN END ### (time taken: %0.4f s)\n" % (time() - turn_start, ))
          self.turn_done()
        else:
          self.universe.update(line)
    except KeyboardInterrupt:
      self.log("User break!")
    self.log("########### GAME END ########### (Turn count: %d)\n" % self.turn_count)

  def send_fleet(self, source_id, destination_id, ship_count):
    u = self.universe
    src = u.planets[source_id]
    dst = u.planets[destination_id]
    self.universe.send_fleet(source_id, destination_id, ship_count)
    self.log("Send from %s %d(%d) ships to %s" % (str(src), ship_count, src.ship_count, str(dst)))
    src.ship_count -= ship_count
    src.freeShip -= ship_count

  def turn_done(self):
    for source_id, destination_id, ship_count in self.universe._fleets_to_send.values():
      sys.stdout.write("%d %d %d\n" % (source_id, destination_id, ship_count))
    sys.stdout.write("go\n")
    sys.stdout.flush()
    self.universe.turn_done()

  def log(self, msg):
    self.logger.debug(msg)
    pass

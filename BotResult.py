import os, sys, time, threading
import xml.etree.ElementTree

# Not a whole lot that's interesting there. The only thing to pay attention to is that you define the default as a function that creates a value as opposed to the value itself. 
# This seems a little odd but makes sense if you think of how assignment works with arrays and hashes - 
# namely that if we used self[key] = self.default instead of self[key] = self.default() we'd end up with all of our default keys pointing to the same array or hash:
# 
# >>> car_details = Ddict( dict )
# >>> car_details['Ford']['Mustang'] = "red"
# >>> car_details['Ford']['Taurus'] = "blue"
# >>> car_details['Toyota']['Corolla'] = "white"
# >>> car_details
# {'Toyota': {'Corolla': 'white'}, 'Ford': {'Mustang': 'red', 'Taurus': 'blue'}}
# Here are a couple of more examples:
# 
# by_user = Ddict( list )   # 1D dictionary with array values
# by_user_cmd = Ddict( lambda: Ddict( list ) )     # 2D dictionary with array values
# user_totals = Ddict( lambda: Ddict( lambda: 0.0 ) )    # 2D dictionary with float values
# user_cmd_totals = Ddict( lambda: Ddict( lambda: Ddict( lambda: 0.0 ) ) )    # 3D dictionary with float values
class Ddict(dict):
  def __init__(self, default=None):
    self.default = default

  def __getitem__(self, key):
    if not self.has_key(key):
      self[key] = self.default()
    return dict.__getitem__(self, key)

custom_log = 'pwars.log'

class BotThread(): #(threading.Thread):

  def __init__(self, callback, bot, maps, results):
    self.callback = callback
    self.bot = bot
    self.maps = maps
    self.results = results
#    threading.Thread.__init__(self)

#  def run(self):
  def start(self):
    callback = self.callback
    rslt = callback(self.bot, self.maps)
    node = xml.etree.ElementTree.Element(self.bot)
    self.results.root.append(node)
    for m in rslt.keys():
      n = xml.etree.ElementTree.Element("map%d" % m)
      n.set('MapNumber', str(m))
      turns, log = rslt[m]
      n.set('Turns', str(turns))
      n.text = log
      node.append(n)

class Err(Exception):
  def __init__(self, message):
    self.message = message
  def __str__(self):
    return repr(self.message)

class Results (xml.etree.ElementTree.ElementTree):

  def __init__(self, file=None):

    if file:
      if not os.path.exists(file):
        raise Err, ("File not found" + ":\n" + file.encode())
      xml.etree.ElementTree.ElementTree.__init__(self, file=file)
      self.root = self.getroot()
      if self.root.tag != 'results':
        raise Err, "Wrong XML header"
      return

    self.root = xml.etree.ElementTree.Element('results')
    xml.etree.ElementTree.ElementTree.__init__(self, self.root)

  def __repr__(self):
    s = "Test time: %s sec\n" % self.root.get('TestTime')
    for bot in self.root:
      s += "%s\n" % self.BotResult(bot)
    return s

  def testBots (self, bots, maps):
    test_time = time.time()
    for bot in bots:
      BotThread(result, bot, maps, self).start()
    while threading.activeCount() > 1: 
      time.sleep(1)
    test_time = time.time() - test_time
    self.root.set('TestTime', "%0.4f" % test_time)

  def BotResult (self, bot):
    s = bot.tag
    for m in bot:
      s += " %s:%s" % (m.get('MapNumber'), m.get('Turns'))
    return s

  def BotSummary (self, bot):
    winner_cnt = 0
    defeat_cnt = 0
    draw_cnt   = 0
    winner_trn = 0
    winner_min = 1000
    winner_max = 0
    defeat_trn = 0
    defeat_min = 1000
    defeat_max = 0
    defeats = []
    for m in bot:
      turn = int(m.get('Turns'))
      if turn > 0:
        winner_cnt += 1
        winner_trn += turn
        if turn > winner_max: winner_max = turn
        if turn < winner_min: winner_min = turn
      elif turn < 0:
        turn = -turn
        defeat_cnt += 1
        defeat_trn += turn
        if turn > defeat_max: defeat_max = turn
        if turn < defeat_min: defeat_min = turn
        defeats.append(int(m.get('MapNumber')))
      else:
        draw_cnt += 1

    return [winner_cnt, draw_cnt, defeat_cnt, winner_max, (0 if winner_cnt == 0 else float(winner_trn) / winner_cnt), winner_min, defeat_max, (0 if defeat_cnt == 0 else float(defeat_trn) / defeat_cnt), defeat_min] + defeats

  def Summary (self):
    s = "Test time: %s sec\n\n" % self.root.get('TestTime')
    for bot in self.root:

      res = self.BotSummary(bot)
      winner_cnt, draw_cnt, defeat_cnt, winner_max, winner_mdl, winner_min, defeat_max, defeat_mdl, defeat_min = res[:9]
      defeats = res[9:]

      sb = "%-16s: %2d/%2d/%2d" % (bot.tag, winner_cnt, draw_cnt, defeat_cnt)
      if winner_cnt > 0:
        sb += " win: %3d/%0.1f/%3d" % (winner_max, winner_mdl, winner_min)
      if defeat_cnt > 0:
        sb += " defeat: %3d/%0.1f/%3d maps:" % (defeat_max, defeat_mdl, defeat_min)
        for m in defeats:
          sb += " %2d" % m
      s += "%s\n" % sb
    return s

  def Compare(self, result_next):
    s = "Compare results\n\n"
    for bot in self.root:

      bot1 = result_next.root.find(bot.tag)
      if bot1:

        res = self.BotSummary(bot)
        res1 = result_next.BotSummary(bot1)

        winner_cnt, draw_cnt, defeat_cnt, winner_max, winner_mdl, winner_min, defeat_max, defeat_mdl, defeat_min = res[:9]
        winner_cnt1, draw_cnt1, defeat_cnt1, winner_max1, winner_mdl1, winner_min1, defeat_max1, defeat_mdl1, defeat_min1 = res1[:9]
  
        sb = "%-16s: %s/%s/%s" % (bot.tag, comp2(winner_cnt, winner_cnt1), comp2(draw_cnt, draw_cnt1), comp2(defeat_cnt, defeat_cnt1))
        if winner_cnt > 0:
          sb += " win: %s/%s/%s" % (comp3(winner_max, winner_max1), compf(winner_mdl, winner_mdl1), comp3(winner_min, winner_min1))
        if defeat_cnt > 0:
          sb += " defeat: %s/%s/%s" % (comp3(defeat_max, defeat_max1), compf(defeat_mdl, defeat_mdl1), comp3(defeat_min, defeat_min1))

        s += "%s\n" % sb

    return s

  def ViewGame(self, bot, mapnum):
    node = self.root.find(bot)
    mapnode = "map%d" % mapnum
    if node:
      for m in node:
        if m.tag == mapnode:
          logname = "%s_%d.log" % (bot, mapnum)
          log = open(logname, 'w')
          log.write(m.text)
          log.close()
          os.system("type. %s | java -jar tools/ShowGame-1.2.jar" % logname)
          os.unlink(logname)
          return

      print "Map %d not found in bot %s results" % (mapnum, bot)
    else:
      print "Bot %s not found in results" % bot

def result(bot, maps):
  res = {}
  for mapnum in maps:
    mapname = "map%d.txt" % mapnum
    viewer_log = "cmd_%s_%d.txt" % (bot, mapnum)
    sys.stdout.write('.')
    os.system("java -jar tools/PlayGame-1.2.jar maps/%s 1000 1000 log_%s.txt \"java -jar example_bots/%s.jar\" \"C:\Python25\python MyBot.py\" 2>%s.log >%s" % (mapname, bot, bot, bot, viewer_log))
    logname = "%s.log" % bot

    f = open(logname, 'r')
    lines = f.readlines()
    f.close()
    os.unlink(logname)

    f = open(viewer_log, 'r')
    cmds = f.readline()
    f.close()
    os.unlink(viewer_log)

    if os.path.exists(custom_log):
      os.rename(custom_log, "logs/%s_%s" % (bot, mapname))

    l = len(lines)
    l1 = lines[l-1]
    l2 = lines[l-2]

    if l1.startswith("Player "):
      win = int(l1.split()[1])
    elif l1.startswith("Draw!"):
      win = 0
    else:
      win = l1.strip()

    if l2.startswith("Turn "):
      turn = int(l2.split()[1])
    else:
      turn = l2.strip()

    res_turns = str(win)
    if type(win).__name__ == 'int':
      if win == 0:
        res_turns = 0
      else:
        res_turns = turn * (1 if win == 2 else -1)

    res[mapnum] = [res_turns, cmds]

  return res

def comp2(v1, v2):
  if v1 < v2:
    return "%+2d" % (v2 - v1)
  elif v1 > v2:
    return "%2d" % (v2 - v1)
  else:
    return " 0"

def comp3(v1, v2):
  if v1 < v2:
    return "%+3d" % (v2 - v1)
  elif v1 > v2:
    return "%3d" % (v2 - v1)
  else:
    return "  0"

def compf(v1, v2):
  if v1 < v2:
    return "%+0.1f" % (v2 - v1)
  elif v1 > v2:
    return "%0.1f" % (v2 - v1)
  else:
    return "   0"

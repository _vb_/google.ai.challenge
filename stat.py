#!/usr/bin/env python
# -*- coding: windows-1251 -*-

import sys, os
from BotResult import Results

#bots = ['BullyBot', 'DualBot', 'ProspectorBot', 'RageBot', 'RandomBot']
bots = ['RageBot', 'RandomBot']
#maps = range(1, 100)
maps = [15, 27]

def main():
  print "Google AI contest runner (C) 2010 by Vitaly Bogomolov"
  if len(sys.argv) < 2:
    print "Usage: stat.py result.xml [{verbose} | {view BotName MapNum} | {compare another_result.xml}]"
    sys.exit(2)

  rslt = None
  xml_file = sys.argv[1]
  if os.path.exists(xml_file):
    rslt = Results(xml_file)
  else:
    rslt = Results()
    rslt.testBots(bots, maps) 
    rslt.write(xml_file, 'cp1251')
    print

  if len(sys.argv) == 2:
    print rslt.Summary()
  else:
    if sys.argv[2] == 'view':
      rslt.ViewGame(sys.argv[3], int(sys.argv[4]))
    elif sys.argv[2] == 'verbose':
      print rslt
    elif sys.argv[2] == 'compare':
      compare = Results(sys.argv[3])
      print rslt.Compare(compare)

if __name__ == '__main__' :
  main()
